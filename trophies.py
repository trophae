#!/usr/bin/python
#
# PSN Trophy Card for Maemo 5
#
# The code is ugly because I hacked it together quick.
#                       -- thp, circa 2010-08-26
#
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
#
#
# Based on:
# PSN Public Trophy List Parser
# 2010-08-24 Thomas Perl <thp@thpinfo.com>
# see also: http://talk.maemo.org/showthread.php?t=59732

import socket
import sys
import re
from BeautifulSoup import BeautifulSoup

import urllib2
import gtk
import pango
import gobject

import os
import hashlib

try:
    import hildon
    ScrolledWindow = hildon.PannableArea
    Window = hildon.StackableWindow
    def Button(text):
        b = hildon.Button(gtk.HILDON_SIZE_AUTO_WIDTH | \
                gtk.HILDON_SIZE_FINGER_HEIGHT, \
                hildon.BUTTON_ARRANGEMENT_VERTICAL)
        b.set_title(text)
        return b
    Entry = lambda: hildon.Entry(0)
except:
    ScrolledWindow = gtk.ScrolledWindow
    Window = gtk.Window
    Button = gtk.Button
    Entry = gtk.Entry

LEVEL_ICON = 'http://us.playstation.com/playstation/img/profile_level_icon.png'

TROPHY_ICONS = (
        'http://us.playstation.com/playstation/img/trophy-01-bronze.png',
        'http://us.playstation.com/playstation/img/trophy-02-silver.png',
        'http://us.playstation.com/playstation/img/trophy-03-gold.png',
        'http://us.playstation.com/playstation/img/trophy-04-platinum.png',
)

def create_connection(address):
    """Stolen from Python 2.6"""
    msg = "getaddrinfo returns an empty list"
    host, port = address
    for res in socket.getaddrinfo(host, port, 0, socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        sock = None
        try:
            sock = socket.socket(af, socktype, proto)
            sock.connect(sa)
            return sock

        except error, msg:
            if sock is not None:
                sock.close()

    raise error, msg

def Progress(window, progress):
    try:
        hildon.hildon_gtk_window_set_progress_indicator(window, progress)
    except:
        pass

def hash(d):
    h = hashlib.sha1()
    h.update(d)
    return h.hexdigest()

def GetContents(url):
    """downloader with caching"""
    if url.startswith('/playstation/PSNImageServlet?avtar='):
        url = url[len('/playstation/PSNImageServlet?avtar='):]
    basename = hash(url)
    filename = os.path.join(os.path.expanduser('~/.cache/ps3trophy/'), basename)

    if not os.path.exists(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename))

    if os.path.exists(filename):
        return open(filename, 'rb').read()
    else:
        data = urllib2.urlopen(url).read()
        fp = open(filename, 'wb')
        fp.write(data)
        fp.close()
        return data

def Header(text):
    return gtk.Label(text)

def Label(text):
    return gtk.Label(text)

def BigLabel(text, cb=None):
    l = gtk.Label()
    l.set_markup('<b><big>%s</big></b>' % (text,))
    if cb is not None:
        cb(l)
    return l

def ProgressIndicator(progress):
    pb = gtk.ProgressBar()
    pb.set_text('%d%%' % (progress,))
    pb.set_fraction(float(progress)/100.)
    return pb

class Game(object):
    def __init__(self, image, name, progress, trophies, \
            *trophycount):
        self.image = image
        self.name = name
        self.progress = progress
        self.trophies = trophies
        self.trophycount = trophycount

    def __unicode__(self):
        name = self.name.replace('\n', ' ').replace('\r', ' ')
        if len(name) > 40:
            name = name[:37] + '...'
        return u'%-40s %3d%% - %2d trophies (%r)' % (name, self.progress, self.trophies, self.trophycount)

class PsnTrophyParser(object):
    def __init__(self, username):
        self.username = username
        self.trophy_data = self.get_trophy_data()
        self.profile_data = self.get_profile_data()
        self.games = list(self.get_games())
        self.parse_profile()

    def get_session_id(self):
        c = create_connection(('us.playstation.com', 80))
        c.send("""HEAD /playstation/psn/profiles/%s HTTP/1.1\r
TE: deflate,gzip;q=0.3\r
Connection: TE, close\r
Host: us.playstation.com\r
User-Agent: lwp-request/5.834 libwww-perl/5.834\r
\r
"""%self.username)
        response = c.recv(1024)
        c.close()
        for line in response.splitlines():
            m = re.match('Set-Cookie: ([^=]*)=([^;]*)', line)
            if m is not None:
                name, value = m.groups()
                if name == 'JSESSIONID':
                    return value

    def get_profile_data(self):
        c = create_connection(('us.playstation.com', 80))
        c.send("""GET /playstation/psn/profiles/%s HTTP/1.0\r
Referer: http://us.playstation.com/publictrophy/index.htm?onlinename=%s\r
User-Agent: Wget/1.12 (linux-gnu)\r
Accept: */*\r
Host: us.playstation.com\r
Connection: Keep-Alive\r
Cookie: JSESSIONID=%s\r
\r
""" % (self.username, self.username, self.get_session_id()))
        response = []
        d = c.recv(1024)
        while d:
            response.append(d)
            d = c.recv(1024)
        c.close()
        return ''.join(response)


    def get_trophy_data(self):
        c = create_connection(('us.playstation.com', 80))
        c.send("""GET /playstation/psn/profile/%s/get_ordered_trophies_data HTTP/1.0\r
Referer: http://us.playstation.com/playstation/psn/profiles/%s\r
User-Agent: Wget/1.12 (linux-gnu)\r
Accept: */*\r
Host: us.playstation.com\r
Connection: Keep-Alive\r
Cookie: JSESSIONID=%s\r
\r
""" % (self.username, self.username, self.get_session_id()))
        response = []
        d = c.recv(1024)
        while d:
            response.append(d)
            d = c.recv(1024)
        c.close()
        return ''.join(response)

    def parse_profile(self):
        s = BeautifulSoup(self.profile_data)
        self.level = int(s.find('div', {'id':'leveltext'}).string.strip())
        self.progress = int(s.find('div', {'class':'progresstext'}).string.strip()[:-1]) # strip the trailing "%" sign
        self.totaltrophies = int(s.find('div', {'id': 'totaltrophies'}).find('div', {'id': 'text'}).string.strip())
        self.avatar = s.find('div', {'id': 'id-avatar'}).find('img')['src']

    def get_games(self):
        s = BeautifulSoup(self.trophy_data)

        for div in s.findAll('div', {'class': 'slot'}):
            image = dict(div.find('img').attrs)['src']
            name = div.find('span', {'class': 'gameTitleSortField'}).string
            progress = int(div.find('span', {'class': \
                    'gameProgressSortField'}).string.strip())
            trophies = int(div.find('span', {'class': \
                    'gameTrophyCountSortField'}).string.strip())
            values = [int(d.string.strip()) for d in div.find('div', {'class': 'trophyholder trophycountholder'}).findAll('div', {'class': 'trophycontent'})]
            yield Game(image, name, progress, trophies, *values)

class TrophyCard(gtk.Table):
    def __init__(self, parser, parent):
        gtk.Table.__init__(self, 4+len(parser.games), 8)
        self._run_later = []
        self._parent = parent
        self.parser = parser
        self.attach(self.Image(self.parser.avatar), 0, 2, 0, 1)
        self.attach(BigLabel(self.parser.username), 0, 2, 1, 2)
        self.attach(self.LevelIndicator(self.parser.level), 2, 4, 0, 1)
        self.attach(ProgressIndicator(self.parser.progress), 2, 7, 1, 2)
        self.attach(BigLabel('%d Trophies' % (parser.totaltrophies,)), 4, 7, 0, 1)
        self.attach(gtk.HSeparator(), 0, 7, 2, 3)
        self.attach(Header('Title'), 0, 1, 3, 4)
        self.attach(Header('Progress'), 1, 2, 3, 4)
        self.attach(Header('Trophies'), 2, 3, 3, 4)
        for i in range(4):
            self.attach(self.Image(TROPHY_ICONS[i]), 3+i, 4+i, 3, 4)

        for i, game in enumerate(self.parser.games):
            self.attach(self.Title(game.name, game.image), 0, 1, 4+i, 5+i)
            self.attach(ProgressIndicator(game.progress), 1, 2, 4+i, 5+i)
            self.attach(Label(str(game.trophies)), 2, 3, 4+i, 5+i)
            for x in range(4):
                self.attach(Label(str(game.trophycount[x])), 3+x, 4+x, 4+i, 5+i)

        gobject.idle_add(self._run_later_once)

    def _run_later_once(self):
        if self._run_later:
            task = self._run_later.pop(0)
            task()
            while gtk.events_pending():
                gtk.main_iteration(False)
        if not self._run_later:
            Progress(self._parent, False)
            return False
        else:
            return True

    def Image(self, url, cb=None):
        i = gtk.Image()
        def x():
            pbl = gtk.gdk.PixbufLoader()
            pbl.write(GetContents(url))
            pbl.close()
            i.set_from_pixbuf(pbl.get_pixbuf())
            if cb is not None:
                cb(i)
        self._run_later.append(x)
        return i

    def ScaledImage(self, url, factor, cb=None):
        def x(i):
            pb = i.get_pixbuf()
            width, height = pb.get_width(), pb.get_height()
            i.set_from_pixbuf(pb.scale_simple(\
                    int(float(width)*factor), \
                    int(float(height)*factor), \
                    gtk.gdk.INTERP_NEAREST))
        im = self.Image(url, x)
        return im

    def LevelIndicator(self, level):
        h = gtk.HBox()
        h.add(self.Image(LEVEL_ICON, lambda i: i.set_alignment(1., .5)))
        h.add(BigLabel(str(level), lambda l: l.set_alignment(0., .5)))
        return h

    def Title(self, text, icon):
        hb = gtk.VBox()
        hb.add(self.ScaledImage(icon, .4))
        lb = gtk.Label()
        lb.set_markup('<small><b>%s</b></small>' % (text,))
        lb.set_ellipsize(pango.ELLIPSIZE_END)
        #hb.add(lb)
        return hb


if __name__ == '__main__':
    def show_trophies(username):
        print 'show_trophies', username
        w = Window()
        w.set_title('Trophies: ' + username)
        w.show_all()
        Progress(w, True)
        while gtk.events_pending():
            gtk.main_iteration(False)

        parser = PsnTrophyParser(username)
        w.resize(800, 580)
        sw = ScrolledWindow()
        sw.add_with_viewport(TrophyCard(parser, w))
        w.add(sw)
        w.show_all()

    w1 = Window()
    w1.set_title('PS3 Trophies')
    hb = gtk.HBox()
    hb.pack_start(gtk.Label('PSN Nickname:'), False, False)
    eUsername = Entry()
    hb.pack_start(eUsername)
    btn = Button('Show trophy card')
    btn.connect('clicked', lambda b: show_trophies(eUsername.get_text()))
    hb.pack_start(btn, False, False)
    vb = gtk.VBox()
    vb.pack_start(gtk.Label(''), True, True)
    vb.pack_start(hb, False, False)
    footer = gtk.Label()
    footer.set_markup('<small>(c) 2010 Thomas Perl &lt;thpinfo.com&gt; / Icon by mr_xzibit</small>')
    footer.set_alignment(1., 1.)
    vb.pack_start(footer, True, True)
    w1.add(vb)
    w1.connect('destroy', gtk.main_quit)
    w1.show_all()

    gtk.main()


